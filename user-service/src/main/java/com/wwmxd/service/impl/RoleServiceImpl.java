package com.wwmxd.service.impl;

import com.wwmxd.entity.Role;
import com.wwmxd.dao.RoleDao;
import com.wwmxd.service.RoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WWMXD
 * @since 2018-01-03 14:39:38
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleDao, Role> implements RoleService  {
	
}
